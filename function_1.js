//Ejercicio 4 y 4.1
class persona{
    constructor(nombre,edad,cedula,sexo,peso,altura){
        this.nombre = nombre
        this.altura = altura
        this.edad = edad
        this.sexo = sexo
        this.cedula = cedula
        this.peso = peso    
    }
    calcularIMC(){
        this.imc =  (this.peso/(this.altura * this.altura))
        if(this.imc < 20){
            return -1
        }
        if(this.imc >= 20 && this.imc <= 25){
            return 0
        }
        if(this.imc > 25){
            return 1
        }
    }
    mayor(){
        if(this.edad >= 18){
            return true
        }
        else{
            return false
        }
    }
    sexo(){
        if(this.sexo == "H" || this.sexo != "M"){
            this.sexo = "H"
            console.log("Es hombre")
        }
        else{
            console.log("Es mujer")
        }
    }
}

let person1 = new persona("juan",29,"102847323","H",52,172)
console.log(person1.mayor())
person1.sexo()
//Ejercicio 5  y 5.1
class contraseña{
    constructor(longitud,contrasena){
        this.longitud = longitud
        this.contrasena = contrasena
    }
    esFuerte(){
        if(this.contraseña.length > 5){
            return "Es Segura"
        }
    }
    generarPassword(tamaño){
        let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz"        
        let contrasenaGenerada = ''
        for (let i=0; i<tamaño; i++) {
            let rnum = Math.floor(Math.random() * chars.length)
            contrasenaGenerada += chars.substring(rnum,rnum+1)
        }
        return contrasenaGenerada
    }
}
//Ejercicio 6  y 7 
class contador{
    constructor(){
        this.conteo = 0;
        this.ultimaacc = "";
    }
    reset(){
        this.conteo = 0;
        this.ultimaacc = "Reiniciar"
    }
    incremento(){
        this.conteo += 1;
        this.ultimaacc = "Sumar"
    }
    decremento(){
        this.conteo -= 1;
        this.ultimaacc = "Restar"
    }
    actualizacion(nuevoValor){
        if(nuevoValor != null){
            this.conteo = nuevoValor;
            this.ultimaacc = "Mostrar"
        }
        if(nuevoValor == null){
            console.log(`valor :  ${this.conteo}`)
        }
    }
    historial(){
        console.log(`La ultima funcion fue   ${this.ultimaacc}`)
    }
}
let contadornuevo = new contador();
contadornuevo.historial(10)
contadornuevo.incremento()
contadornuevo.decremento()
contadornuevo.incremento()
contadornuevo.historial()

