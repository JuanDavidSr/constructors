//Ejercicio 11
class libro{
    constructor(titulo,autor,nEjemplares,nEjemplaresPrestados){
        this.titulo = titulo
        this.autor = autor
        this.nEjemplares=nEjemplares
        this.nEjemplaresPrestados = nEjemplaresPrestados
    }
    prestar(){
        if(this.nEjemplares > 0){
            this.nEjemplaresPrestados += 1
            this.nEjemplares -= 1
            return true
        }
        else{
            return false
        }
    }
    devolver(){
        if(this.nEjemplaresPrestados > 0){
            this.nEjemplaresPrestados -= 1
            this.nEjemplares += 1
            return true
        }
        else{
            return false
        }
    }
    toString(){
        console.log(`Titulo: ${this.titulo}`)
        console.log(`Autor: ${this.autor}`)
        console.log(`Ejemplares disponibles: ${ this.nEjemplares}`)
        console.log(`Ejemplares Prestados: ${this.nEjemplaresPrestados}`)
    }
}
//EJERCICIO 12
class enterprise{
    constructor(potencia, coraza){
        this.potencia = 50
        this.coraza = 5
    }
    encontrarPilaAtomica(){
        if(this.potencia >= 100){
            console.log("Potencia Maxima")
        }
        else{
            this.potencia +=  25
        }
        if(this.potencia > 100){
            this.potencia = 100
        }
        console.log(`La potencia actual es de ${this.potencia}`)
    }
    escudo(){
        if(this.coraza >= 20){
            console.log("Escudo Maximo")
        }
        else{
            this.coraza +=  10
        }
        if(this.coraza > 20){
            this.coraza = 20
        }
        console.log(`La coraza actual es de ${this.coraza}`)
    }
    recibirAtaque(puntosDaño){
        let total = this.coraza - puntosDaño
        if(total >= 0){
            this.coraza = total
        }
        if(total < 0){
            this.coraza = 0
            this.potencia -= (total * -1)
        }
    }
    mostrarStats(){
        console.log("los stocks actuales son: ")
        console.log(`La coraza actua es de:  ${this.coraza}`)
        console.log(`La potencia actual es de:  ${this.potencia}`)
    }
}
let nave1 = new enterprise()
nave1.encontrarPilaAtomica()
nave1.recibirAtaque(14)
nave1.escudo()
nave1.mostrarStats()
//Ejercicio 13
class enterprise{
    constructor(potencia, coraza){
        this.potencia = 50
        this.coraza = 5
    }
    encontrarPilaAtomica(){
        if(this.potencia >= 100){
            console.log("Potencia Maxima")
        }
        else{
            this.potencia +=  25
        }
        if(this.potencia > 100){
            this.potencia = 100
        }
        console.log(`La potencia actual es de ${this.potencia}`)
    }
    encontrarEscudo(){
        if(this.coraza >= 20){
            console.log("Escudo Maximo")
        }
        else{
            this.coraza +=  10
        }
        if(this.coraza > 20){
            this.coraza = 20
        }
        console.log(`La coraza actual es de ${this.coraza}`)
    }
    recibirAtaque(puntosDaño){
        let total = this.coraza - puntosDaño
        if(total >= 0){
            this.coraza = total
        }
        if(total < 0){
            this.coraza = 0
            this.potencia -= (total * -1)
        }
    }
    mostrarStats(){
        console.log("los stats actuales son: ")
        console.log(`La coraza actua es de ${this.coraza}`)
        console.log(`La potencia actual es e ${this.potencia}`)
    }
    fortalezaDefensiva(){
        return this.coraza + this.potencia
    }
    necesitaFortalecer(){
        if(this.potencia <= 20 && this.coraza == 0 ){
            return true
        }
        else{
            return false
        }
    }
    fortalezaOfensiva(){
        if(this.potencia < 20){
            return 0
        }
        if(this.potencia > 20){
            return (this.potencia-20)/2
        }
    }
}

//EJERCICIO 14
function caracteristicas_motor(){
    this.cambio = 0;
    this.rpm = 0;
    this.calcular_velocidad = function(){
        return (this.rpm / 100) * (0.5 + (this.cambio / 2));
    }
    this.arrancar = function(){
         this.cambio = 1;
         this.rpm = 500;
    }
    this.subirRPM = function(valor){
         if(valor > 5000) {
             this.rpm = 5000;
         } else if(valor  < 0) {
             this.rpm = 0;
         } else {
             this.rpm = valor;
         }
    }
     this.bajarRPM = function(valor){
         if(valor > 5000) {
             this.rpm = 5000;
         } else if(valor  < 0) {
             this.rpm = 0;
         } else {
             this.rpm = valor;
         }
    }
    this.consumoActualPorKm = function(){
          const baseLitros = 0.05;
          let factorConsumo = (this.rpm - 2500) / 500;
          let base = 0;
          if(this.cambio == 1){
              base = 3;
          } else if(this.cambio == 2){
              base = 2;
          } else {
              base = 1;
          }
          return factorConsumo * base ;
    }
    this.subirCambio = function(){
        this.cambio = this.cambio += 1;
        if(this.cambio > 5) {
            this.cambio = 5;
        }
    }
    this.bajarCambio = function(){
        this.cambio = this.cambio -= 1;
        if(this.cambio < 1) {
            this.cambio = 1;
        }
    }
  }