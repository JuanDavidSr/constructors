//Ejercicio numero 1  y Ejercicio 1.1 
class persona{
    constructor(nombre,edad,identificacion){
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.edad = edad;
    }
    ver(){
        console.log("Informacion personal: ")
        console.log(`Nombre: ${this.nombre}  Cedula: ${this.cedula} Edad: ${this.edad}`)
    }
    mayor() {
        if(this.edad >= 18){
            return true
        }
        if(this.edad >= 0 && this.edad < 18){
            return false
        }
        if(this.edad < 0){
            return "Ingrese una edad mayor de 0"
        }
    }
}
let nombre = "Pepito" // prompt("ingresa un nombre")
let edad = 19 //parseInt(prompt("ingresa una edad"))
let cedula = 12345568 // prompt("ingresa una cedula")
let persona1 = new persona(nombre, edad, cedula)
persona1.ver();
console.log(persona1.mayor())

//Ejercicio 2 y //Ejercicio 2.2
class cuenta{
    constructor(titular,saldo) {
        this.titular = titular
        this.cantidad =saldo
    }
    ver() {
        console.log(`Titular: ${this.titular} Saldo: ${this.cantidad}`)
    }
    nuevo(valor){
        if(valor > 0){
            this.cantidad += valor; // this.cantidad = this.cantidad + valor
            console.log("Consignación exitosa")
        }
        if(valor <= 0){
            console.log("No se pudo procesar tu solicitud.")
            console.log("El valor ingresado no es valido.")
        }}
    retirar(valor){
        if(valor <= this.cantidad){
            this.cantidad -= valor;
            console.log("Retiro exitoso")
            console.log(`Tu saldo actual es : ${this.cantidad}`)
        }
        if(valor > this.cantidad){
            console.log("Saldo insuficiente")
            console.log(`Tu saldo es de : ${this.cantidad}`)
        }
    }
}
let cuentaJuan = ["Juan",300500]
let cuenta1 = new cuenta(cuentaJuan[0],cuentaJuan[1])
cuenta1.nuevo(10000)
cuenta1.retirar(45000)
cuenta1.ver()
//Ejercicio 3 y ejercicio 3.1
class formulas{
    sumar(num1,num2){
        return num1+num2
    }
    fibonaci(cantidad){
        let fibo =[0,1]
        for(i = 2; i<=cantidad; i++){
            fibo.push(fibo[i-1]+fibo[i-2])
            console.log(fibo[i])
        }
    }
    modulo(cantidad){
        for(i = 1;i <= cantidad;i++){
            if(i % 2 == 0){
                console.log(i)
            }
        }
    }
    primos(cantidad){
        let validador = false
        for(let i = 0; i < cantidad;i++){
            for(let x = 2; x < i ;x++){
                if(i % x === 0){
                    validador = false
                }
                else{
                    validador = true
                }
            }
            if(validador){
                console.log(`El número ${i} es primo`)
            }
            else{
                console.log(`El número ${i} no es primo`)
            }
        }
    }
}
let calculo = new formulas()
console.log(calculo.sumar(11,42))
calculo.primos(2)



